package org.fjala.inventory;

public class Category implements Catalog {

    private String name;
    public Category(String name) {
        this.name = name;
    }

    public SubCategory createSubcat(String name) {
        
        return new SubCategory(getName(), "bread");
    }
    
    @Override
    public String getName() {
        
        return name;
    }

    
    
}