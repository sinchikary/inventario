package org.fjala.inventory;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Test;

public class TestInventory {
    @Test 
    public void returnTypesOfProducts() {
        Catalog catalog1 = new Category("Food");
        String name = catalog1.getName();

        assertThat(name, is("Food"));
    }
}